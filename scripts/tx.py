#!/usr/bin/env python3
"""Play a sine signal."""
import argparse
import sys
from time import sleep

import numpy as np
import sounddevice as sd

def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument(
    '-l', '--list-devices', action='store_true',
    help='show list of audio devices and exit')
args, remaining = parser.parse_known_args()
if args.list_devices:
    print(sd.query_devices())
    parser.exit(0)
parser = argparse.ArgumentParser(

    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[parser])
parser.add_argument(
    'frequency', nargs='?', metavar='FREQUENCY', type=float, default=500,
    help='frequency in Hz (default: %(default)s)')
parser.add_argument(
    'frequency2', nargs='?', metavar='FREQUENCY2', type=float, default=800,
    help='frequency in Hz (default: %(default)s)')
parser.add_argument(
    '-d', '--device', type=int_or_str,
    help='output device (numeric ID or substring)')
parser.add_argument(
    '-a', '--amplitude', type=float, default=0.2,
    help='amplitude (default: %(default)s)')
args = parser.parse_args(remaining)

start_idx = 0
toggle_freq1 = False
toggle_freq2 = False

try:
    samplerate = sd.query_devices(args.device, 'output')['default_samplerate']

    def callback(outdata, frames, time, status):
        if status:
            print(status, file=sys.stderr)
        global start_idx, toggle_freq2
        t = (start_idx + np.arange(frames)) / samplerate
        t = t.reshape(-1, 1)

        outdata[:] = args.amplitude * (np.sin(2 * np.pi * args.frequency * toggle_freq1 * t) +
                                       np.sin(2 * np.pi * args.frequency2 * toggle_freq2 * t))
        start_idx += frames

    def playString(string):
        global toggle_freq1, toggle_freq2
        for char in string:
            if char == '1':
                toggle_freq1 = True
            else:
                toggle_freq1 = False
            sleep(0.1)
            toggle_freq2 = True
            sleep(0.2)
            toggle_freq2 = False
            sleep(0.1)
            toggle_freq1 = False

    with sd.OutputStream(device=args.device, channels=1, callback=callback,
                         samplerate=samplerate):
        while True:
            try:
                value = input()
                playString(value)
            except KeyboardInterrupt:
                break
except KeyboardInterrupt:
    parser.exit('')
except Exception as e:
    parser.exit(type(e).__name__ + ': ' + str(e))
