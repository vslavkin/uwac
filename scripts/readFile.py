"""PyAudio Example: Play a wave file."""

import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import numpy as np
from scipy.fft import fft, fftfreq
from scipy.io.wavfile import read
import sounddevice as sd
import sys

# axis[0].plot(xf, 2.0/N * yf[0:N//2])
# axis[0].grid()
# axis[1].plot(x, y)
# axis[1].grid()

# read audio samples
# input_data = read(sys.argv[1])
# print(input_data[0])
# audio = input_data[1]

class waveSignal:
    def __init__ (self, filePath):
        self.filePath = filePath
        wav_data = read(filePath)
        self.sampleRate = wav_data[0]
        self.data = wav_data[1]

    def getData (self):
        return self.data
    def getSampleRate (self):
        return self.sampleRate

class buttonActions:
    def __init__ (self, wavePlot):
        self.wavePlot = wavePlot

    def nextStep (self, event):
        self.wavePlot.shiftGraph(1)

    def previousStep (self, event):
        self.wavePlot.shiftGraph(-1)

    def playAction (self, event):
        self.wavePlot.playAcSection()

    def fourierAction (self, event):
        self.wavePlot.updateFourier()

class wavePlot:
    def __init__ (self, filePath):
        self.fig, self.ax = plt.subplots(2)
        self.fig.subplots_adjust(bottom=0.2)
        # label the axes
        plt.ylabel("Amplitude")
        plt.xlabel("Time")
        plt.title("Sample Wav") # set the title

        self.chunkSize = 1024
        self.signal = waveSignal(filePath)
        # ax.plot(signal.getDataSubset(actions.getStep()*CHUNK, (actions.getStep()+1)*CHUNK))
        # self.plotData, = self.ax.plot(self.signal.getDataSubset(0, self.chunkSize))
        self.plotData, = self.ax[0].plot(self.signal.getData())
        self.ax[0].set_xlim(left=0)
        self.updateFourier()

        self.actions = buttonActions(self)
        self.axisNextBtn = self.fig.add_axes([0.81, 0.05, 0.1, 0.075])
        self.BtnNext = Button(self.axisNextBtn, 'Next')
        self.BtnNext.on_clicked(self.actions.nextStep)
        self.axisPrevBtn = self.fig.add_axes([0.7, 0.05, 0.1, 0.075])
        self.BtnPrev = Button(self.axisPrevBtn, 'Previous')
        self.BtnPrev.on_clicked(self.actions.previousStep)
        self.axisPlayBtn = self.fig.add_axes([0.59, 0.05, 0.1, 0.075])
        self.BtnPlay = Button(self.axisPlayBtn, 'Play')
        self.BtnPlay.on_clicked(self.actions.playAction)
        self.axisFourBtn = self.fig.add_axes([0.4, 0.05, 0.1, 0.075])
        self.BtnFour = Button(self.axisFourBtn, 'Fourier')
        self.BtnFour.on_clicked(self.actions.fourierAction)
        # display the plot
        plt.show()

    def updateFourier (self):
        self.ax[1].clear()
        xstart, xend = self.ax[0].get_xlim()
        N = int(xend) - int(xstart)
        self.fourier = self.ax[1].plot(fftfreq(N, 1/N)[:self.signal.getSampleRate()//2],
                                       np.abs(fft(self.signal.getData()[int(xstart):int(xend)])[0:self.signal.getSampleRate()//2]))
        self.ax[1].plot()
        self.updatePlot()

    def shiftGraph (self, shift):
        oldLeft, oldRight = self.ax[0].get_xlim()
        chunk = abs(oldRight-oldLeft)
        start = oldLeft + chunk * shift
        end = start + chunk
        self.ax[0].set_xlim(start,end)
        self.updatePlot()

    def updatePlot(self):
        # self.plotData.set_data(np.arange(start,start+self.chunkSize),self.signal.getDataChunk(start, self.chunkSize))
        # self.ax[0].autoscale_view()
        self.fig.canvas.draw()

    def playAcSection(self):
        start, end = self.ax[0].get_xlim()
        start = int(start)
        end = int(end)
        audio = self.signal.getData()
        sd.play(audio[start:end], samplerate=self.signal.getSampleRate())

    # def updateFourier(self):
    #     yf = fft(y)
    #     xf = fftfreq(N, 1/N)[:N//2]
    #     self.ax[1].plot(xf, 2.0/N * yf[0:N//2])
    #     self.ax.draw()
    #     # self.updatePlot()


wavePlot(sys.argv[1])
