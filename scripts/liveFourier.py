#-------------------------SAR Highperformance-----------------------
#------------------------Spectrogram Generator----------------------

import pyaudio
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy import signal
from scipy.fft import fft, fftfreq
from scipy.fft import rfft, rfftfreq

def get_closest_freq_index(freq, rate, chunk):
    """Find the index in the FFT result that corresponds to the target frequency."""
    return int(freq / rate * chunk)

# Set the audio parameters
RATE = 44100
CHUNK = 2048

# Initialize PyAudio and open the audio stream
p = pyaudio.PyAudio()
stream = p.open(
    format=pyaudio.paInt16,
    channels=1,
    rate=RATE,
    input=True,
    frames_per_buffer=CHUNK
)

# Create the figure and axes for the plot
fig, ax = plt.subplots(2)
x = np.arange(CHUNK)
# line, = ax.plot(x,0)

# Function to update the spectrogram
def update_spectrogram(frame):
    audio_array = np.frombuffer(stream.read(CHUNK), dtype=np.int16)
    # frequencies, times, Sxx = signal.spectrogram(audio_array, RATE)
    ax[0].clear()
    ax[1].clear()
    # ax.pcolormesh(times, frequencies, 10 * np.log10(Sxx), shading='auto', cmap='inferno')
    ax[0].plot(x,audio_array)

    xfft = np.abs(rfft(audio_array))

    ax[1].plot(rfftfreq(CHUNK, 1/RATE),
               xfft)
    ax[1].set_xlim(0,2000)

    # line.set_ydata(audio_array)
    ax[1].set_xlabel('Frequency [Hz]')
    ax[1].set_ylabel('Amplitude')

    freqIdx = get_closest_freq_index(440, RATE, CHUNK)
    if xfft[freqIdx] > 5000:
        print("440")

    return ax[0], ax[1],


# Create the animation
ani = animation.FuncAnimation(fig, update_spectrogram, interval=1/(RATE/CHUNK), blit=False)

# Show the plot
plt.show()

# Close the audio stream
stream.stop_stream()
stream.close()
p.terminate()
