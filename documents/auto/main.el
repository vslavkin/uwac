(TeX-add-style-hook
 "main"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("apa7" "a4paper" "stu" "biblatex")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("babel" "spanish")))
   (TeX-run-style-hooks
    "latex2e"
    "apa7"
    "apa710"
    "hyperref"
    "inputenc"
    "babel"
    "graphicx"
    "csquotes")
   (LaTeX-add-labels
    "fig:espectroAbsorcion"
    "tab:BasicTable"
    "fig:Figure1"
    "app:instrument"
    "fig:Figure2"
    "app:surveydata"
    "tab:DeckedTable")
   (LaTeX-add-bibliographies
    "biblatex"))
 :latex)

