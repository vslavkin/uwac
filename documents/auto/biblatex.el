(TeX-add-style-hook
 "biblatex"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-bibitems
    "opticalAbsorption"
    "recentAdvances"
    "usNavyElf"))
 '(or :bibtex :latex))

